import React from "react";
import styled from "styled-components";
import Particle from "../Components/Particle";

import Linksredes from "../Components/Linksredes";

function HomePage() {
  return (
    <HomePageStyled>
      <div className="particle-con">
        <Particle />
      </div>
      <div className="typography">
        <h1>
          Hola, soy <span>Jorge Vicuña Valle</span>
        </h1>
        <p>Bachiller en Ing. Electrónica y Desarrollador de corazón.</p>
        <Linksredes />
      </div>
    </HomePageStyled>
  );
}

const HomePageStyled = styled.header`
  width: 100%;
  height: 100vh;
  position: relative;

  .typography {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    text-align: center;
    width: 80%;
  }
`;

export default HomePage;
