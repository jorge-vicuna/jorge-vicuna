import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import GlobalStyle from './styles/GlobalStyle';
debugger;

ReactDOM.render(
  <>
    <GlobalStyle />
    <BrowserRouter  basename= "/jorge-vicuna/">
      <App />
    </BrowserRouter>
    
  </>,
  document.getElementById('root')
);

