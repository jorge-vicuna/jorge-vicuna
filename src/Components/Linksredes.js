import React from 'react'
import styled from 'styled-components';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import GithubIcon from '@material-ui/icons/GitHub';
import YoutubeIcon from '@material-ui/icons/YouTube';
import { FaGitlab } from 'react-icons/fa';
import { AiFillLinkedin } from 'react-icons/ai';     
      
function Linksredes() {  
    return(          
        <LinksStyled>
            <div className="icons">
                <a target="_blank" href="https://www.linkedin.com/in/jorge-vicuna-valle/" rel="noreferrer"className="icon i-linkedin">
                    <AiFillLinkedin style={{width:'25'}}  />
                </a>
                <a target="_blank" href="https://gitlab.com/jorge_vicuna" rel="noreferrer" className="icon i-gitlab">
                    <FaGitlab style={{width:'25'}} />
                </a>
                <a target="_blank" href="https://github.com/Jorge150896" rel="noreferrer" className="icon i-github">
                    <GithubIcon />
                </a>
                <a target="_blank" href="https://www.youtube.com/channel/UCW0m1TKKiN3Etejqx6h3Jtg" rel="noreferrer" className="icon i-youtube">
                    <YoutubeIcon />
                </a>
                <a target="_blank" href="https://www.instagram.com/jorge_vicuna_valle/" rel="noreferrer" className="icon i-instagram">
                    <InstagramIcon />
                </a>
                <a target="_blank" href="https://www.facebook.com/jorge.vicunavalle/" rel="noreferrer" className="icon i-facebook">
                    <FacebookIcon />
                </a>
            </div>

        </LinksStyled>
    )
}
const LinksStyled = styled.div`

        .icons{
            display: flex;
            justify-content: center;
            margin-top: 1rem;
            .icon{
                border: 2px solid var(--border-color);
                display: flex;
                align-items: center;
                justify-content: center;
                border-radius: 50%;
                transition: all .4s ease-in-out;
                cursor: pointer;
                &:hover{
                    border: 2px solid var(--primary-color);
                    color: var(--primary-color);
                }
                &:not(:last-child){
                    margin-right: 1rem;
                }
                svg{
                    margin: .5rem;
                }
            }

            .i-youtube{
                &:hover{
                    border: 2px solid red;
                    color: red;
                }
            }
            .i-github{
                &:hover{
                    border: 2px solid #5F4687;
                    color: #5F4687;
                }
            }
            .i-gitlab{
                &:hover{
                    border: 2px solid #f46a25;
                    color: #f46a25;
                }
            }
            .i-linkedin{
                &:hover{
                    border: 2px solid #0a66c2;
                    color: #0a66c2;
                }
            }
            .i-instagram{
                &:hover{
                    border: 2px solid #8c48be;
                    color: #8c48be;
                }
            }
        
        }
    
`;

export default Linksredes;