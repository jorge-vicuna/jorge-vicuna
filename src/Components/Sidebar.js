import React, {useState,useEffect} from 'react'
import styled from 'styled-components';
import BackDrop from './BackDrop/BackDrop';
import Navigation from './Navigation';


function Sidebar(props) {
    
const {navToggle,handleclick}= props
    // const [toggleState, setToggleState] = useState(navToggle);
 
    const handelClosed = ()=>{
        // setNavToggle(!navToggle);
        // console.log(toggleState)
        handleclick()
        console.log(navToggle)
        
        if(navToggle)
            document.body.classList.remove('overflow-hidden');
        else
            document.body.classList.add('overflow-hidden');

    }


    return (
        <>
            {navToggle ? <BackDrop toggleFunc={handelClosed} /> : ''}

        <SidebarStyled className={`${navToggle ? 'nav-toggle' : ''}`}>
        {/* {navToggle ? <BackDrop toggleFunc={toggle} /> : ''} */}

            <Navigation />

        </SidebarStyled>
        </>
    )
}

const SidebarStyled = styled.div`
    width: 16.3rem;
    position: fixed;
    height: 100vh;
    background-color: var(--sidebar-dark-color);
    overflow: hidden;
    transition: all .4s ease-in-out;
    @media screen and (max-width:1200px){
        transform: translateX(-100%);
        z-index: 20;
    }
`;

export default Sidebar;
