import React from "react";
import styled from "styled-components";
import resume from "../img/resume.jpg";

import Linksredes from "./Linksredes";
import PrimaryButton from "./PrimaryButton";

function ImageSection() {
  return (
    <ImageSectionStyled>
      <div className="left-content">
        <img src={resume} alt="" />
      </div>
      <div className="right-content">
        <h4>
          Soy <span>Bachiller Ing. Electrónica</span> -{" "}
          <span>Desarrollador Web/Mobile</span>
        </h4>
        <p className="paragraph">
          Pertenezco a varios grupos innovadores, en los cuales he logrado los
          primeros lugares en eventos de programación y robótica. Asimismo, he
          adquirido una diversidad de conocimientos que ahora pongo en práctica
          en mi creciendo profesional en el sector tecnológico del desarrollo
          web, mobile e integraciones de software con hardware. En la parte web
          he trabajado con tecnologías como React y Angular, soy Full Stack MERN
          (Mongo-Express-React y Node) y en la parte Mobile he desarrollado con
          Swift, Flutter y React Native.
        </p>
        <div className="about-info">
          <div className="info-title">
            <p>Nombre</p>
            <p>Edad</p>
            <p>Idiomas </p>
            <p>Dirección</p>
          </div>
          <div className="info">
            <p>: Jorge Vicuña Valle</p>
            <p>: 25</p>
            <p>: Español, Inglés (Básico)</p>
            <p>: Perú, Lima, Comas</p>
          </div>
        </div>
        <Linksredes />
        <div style={{ display: "flex", justifyContent: "center" }}>
          <a
            target="_blank"
            href="https://drive.google.com/file/d/1xg_CLjwa7yxnIu7m2BuM79cTGaQFmYMt/view?usp=sharing"
            rel="noreferrer"
          >
            <PrimaryButton title={"Download Cv"} />
          </a>
        </div>
      </div>
    </ImageSectionStyled>
  );
}

const ImageSectionStyled = styled.div`
  margin-top: 5rem;
  display: flex;
  @media screen and (max-width: 1000px) {
    flex-direction: column;
    .left-content {
      margin-bottom: 2rem;
    }
  }
  .left-content {
    width: 100%;
    img {
      width: 95%;
      object-fit: cover;
    }
  }
  .right-content {
    width: 100%;
    h4 {
      font-size: 2rem;
      color: var(--white-color);
      span {
        font-size: 2rem;
      }
    }
    .paragraph {
      padding: 1rem 0;
      text-align: justify;
    }
    .about-info {
      display: flex;
      padding-bottom: 1.4rem;
      .info-title {
        padding-right: 3rem;
        p {
          font-weight: 600;
        }
      }
      .info-title,
      .info {
        p {
          padding: 0.3rem 0;
        }
      }
    }
  }
  .icons {
    display: flex;
    justify-content: center;
    margin-top: 1rem;
    .icon {
      border: 2px solid var(--border-color);
      display: flex;
      margin-bottom: 30px;
      align-items: center;
      justify-content: center;
      border-radius: 50%;
      transition: all 0.4s ease-in-out;
      cursor: pointer;
      &:hover {
        border: 2px solid var(--primary-color);
        color: var(--primary-color);
      }
      &:not(:last-child) {
        margin-right: 1rem;
      }
      svg {
        margin: 0.5rem;
      }
    }

    .i-youtube {
      &:hover {
        border: 2px solid red;
        color: red;
      }
    }
    .i-github {
      &:hover {
        border: 2px solid #5f4687;
        color: #5f4687;
      }
    }
    .i-gitlab {
      &:hover {
        border: 2px solid #f46a25;
        color: #f46a25;
      }
    }
    .i-linkedin {
      &:hover {
        border: 2px solid #0a66c2;
        color: #0a66c2;
      }
    }
  }
`;
export default ImageSection;
