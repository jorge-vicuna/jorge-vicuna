import React from "react";
import styled from "styled-components";
import { InnerLayout } from "../styles/Layouts";
import Title from "../Components/Title";
import SmallTitle from "../Components/SmallTitle";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import SchoolIcon from "@material-ui/icons/School";
import ResumeItem from "../Components/ResumeItem";

function Resume() {
  const briefcase = <BusinessCenterIcon />;
  const school = <SchoolIcon />;
  return (
    <ResumeStyled>
      <Title title={"Resume"} span={"resume"} />
      <InnerLayout>
        <div className="small-title">
          <SmallTitle icon={briefcase} title={"Experiencia Laboral"} />
        </div>
        <div className="resume-content">
        <ResumeItem
            year={"2023 Feb - Actualidad"}
            title={"Software Engineer III, IOS"}
            subTitle={"Scotiabank"}
            text={
              "Programador IOS en la empresa Scotiabank, desarrollo Frontend con UIKIT, replicando diseños de interfaces. Implementación del proyecto interoperabilidad en Plin, para transferir a otras billeteras como Yape, Bim, entre otros. Desarrollo con arquitecturas MVVM-C con Combine. Pruebas unitarias de los desarrollos."
            }
          />
          <ResumeItem
            year={"2021 Nov - 2023 Feb"}
            title={"iOS Developer"}
            subTitle={"NTT DATA Perú"}
            text={
              "Programador IOS en la empresa NTT DATA, en el proyecto SmartHome: Desarrollo Frontend con UIKIT, replicando diseños de interfaces. Implementación de nuevas funciones e integraciones de servicios. Desarrollo con arquitecturas MVVM y VIPER. Detección y correcciones de errores que se presenten en las pruebas del app."
            }
          />
          <ResumeItem
            year={"2021 Sep - 2021 Dec"}
            title={"Programador Junior Freelance"}
            subTitle={"Mantis Code"}
            text={
              "Programador Junior Freelance en la empresa Mantis Code: Desarrollo Frontend en proyectos web con Angular, TypeScript, Sass, Material UI, Creación de Lambda function handler en Python para la parte Backend basado en arquitectura Serverless Application Model (SAM). Desarrollo de Stored Procedure en MYSQL/MariaDB."
            }
          />
          <ResumeItem
            year={"2019 Nov - 2021 Jun"}
            title={"Practicante de Operaciones"}
            subTitle={"Junior Achievement Perú"}
            text={
              "Prácticas Pre-Profesionales en la ONG Junior Achievement, en el Área de Operaciones para el desarrollo de material audiovisual en temas de emprendimiento, robótica y programación, soporte técnico en los programas del área y administración de la web de JA Perú y desarrollo de prototipos digitales."
            }
          />
          <ResumeItem
            year={"2018 Mzo - 2018 My"}
            title={"Asistente"}
            subTitle={"NFM  Robotics"}
            text={
              "Prácticas Pre-Profesional en la empresa NFM ROBOTICS como Asistente del proyecto red social “ROBÓTICA LATINA”, plataforma web enfocada en agrupar personas con conocimientos en electrónica, mecatrónica y sistemas"
            }
          />
        </div>
        <div className="small-title u-small-title-margin">
          <SmallTitle icon={school} title={"Educación"} />
        </div>
        <div className="resume-content ">
          <ResumeItem
            year={"2021 Nov - 2022-Jun"}
            title={"Desarrollo Full Stack JavaScript"}
            subTitle={"Make It Real - Innovate Perú"}
            text={
              "Programa que se ejecuta a través un Bootcamp Intensivo de desarrollo Web para convertirse en un Full Stack MERN (Mongo, Express, React, Node) a través clases personalizadas con especialistas y proyectos aplicando los temas aprendidos; asimismo, desarrollar habilidades de trabajo en equipo y dominar la metodología ágil Scrum."
            }
          />
          <ResumeItem
            year={"2021 Jul - 2021 Dec"}
            title={"Ruta de Aprendizaje Developers"}
            subTitle={"Platzi-Innovate Perú"}
            text={
              "Ruta de Aprendizaje para desarrollar habilidades como Front-End Developer, cursos de la plataforma y asesorías en tecnologías como React, Angular, Vue , Node,  entre otros para cumplir con el objetivo del programa."
            }
          />
          <ResumeItem
            year={"2015 Ago - 2020 Jul"}
            title={"Bachiller Ingeniería Electrónica"}
            subTitle={"Universidad Privada del Norte"}
            text={
              "Diseño, desarrollo e implementación de múltiples sistemas electrónicos, manejo de sistemas digitales, programación de microcontroladores, Inteligencia Artificial e Internet de las Cosas."
            }
          />
          <ResumeItem
            year={"2016 - 2017"}
            title={"Inglés Regular Nivel Básico"}
            subTitle={"Universidad Privada del Norte"}
            text={
              "Programa Inglés UPN, respaldado por Edusoft, permite alcanzar el nivel requerido por carrera y se convierte en una herramienta muy importante para el logro de objetivos académicos y profesionales."
            }
          />
          <ResumeItem
            year={"2009 - 2013"}
            title={"Secundaria Completa"}
            subTitle={"I.E.E Carlos Wiesse"}
            text={
              "Primera Institución Educativa Emblemática Pública de Lima Norte, a la vanguardia con las ciencia y tecnología."
            }
          />
        </div>
      </InnerLayout>
    </ResumeStyled>
  );
}

const ResumeStyled = styled.section`
  .small-title {
    padding-bottom: 3rem;
  }
  .u-small-title-margin {
    margin-top: 4rem;
  }

  .resume-content {
    border-left: 2px solid var(--border-color);
  }
`;
export default Resume;
