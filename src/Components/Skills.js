import React from "react";
import styled from "styled-components";
import { InnerLayout } from "../styles/Layouts";
import Title from "../Components/Title";
import ProgressBar from "./ProgressBar";

function Skills() {
  return (
    <SkillsStyled>
      <Title title={"My Skills"} span={"my skills"} />
      <InnerLayout>
        <div className="skills">
          <ProgressBar
            title={
              "Desarrollo de aplicativos móviles (Swift-Flutter)"
            }
            width={"80%"}
            text={"80%"}
          />
          <ProgressBar
            title={"Desarrollo Front-end (React-JavaScript-Html-Css)"}
            width={"70%"}
            text={"70%"}
          />
          <ProgressBar
            title={
              "Conocimientos en PIC, Arduino, NodeMCU, Raspberry y Photon."
            }
            width={"70%"}
            text={"70%"}
          />
          <ProgressBar
            title={"Conocimientos en Back-End (Node-Nest)"}
            width={"60%"}
            text={"60%"}
          />

          <ProgressBar
            title={"Conocimientos Base de datos (MYSQL-MongoDB)"}
            width={"50%"}
            text={"50%"}
          />
          <ProgressBar
            title={"Prototipado (Figma, AdobeXD)"}
            width={"60%"}
            text={"60%"}
          />
          <ProgressBar
            title={"Conocimientos en Git-GitHub-Gitlab"}
            width={"80%"}
            text={"80%"}
          />
          <ProgressBar
            title={"Robótica Educativa (Scratch-App Inventor-Tinkercad)"}
            width={"100%"}
            text={"100%"}
          />
        </div>
      </InnerLayout>
    </SkillsStyled>
  );
}

const SkillsStyled = styled.section`
  .skills {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-row-gap: 2rem;
    grid-column-gap: 3rem;
    @media screen and (max-width: 700px) {
      grid-template-columns: repeat(1, 1fr);
    }
  }
`;

export default Skills;
