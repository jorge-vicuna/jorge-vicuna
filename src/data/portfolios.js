const portfolios = [
  {
    id: 1,
    category: "Flutter",
    image: "https://i.imgur.com/9I8aaY7.jpg",
    link1:
      "https://gitlab.com/proyectos-flutter1/flutter-basico-fh-2023/6-cinemapedia",
    link2: "https://www.youtube.com/watch?v=rZ2rQTlmSYU",
    // link3: 'https://qhipapachacode.gitlab.io/qhipapachacode/',
    title: "Cinemapedia App",
    text: "Cinemapedia App (Flutter)",
  },
  {
    id: 2,
    category: "React Native",
    image: "https://i.imgur.com/pFYIAmD.png",
    link1:
      "https://gitlab.com/reactNative1508/reactnative-platzi-pokedex/pokedex",
    link2: "https://www.youtube.com/watch?v=MdNEiHk2dWE",
    // link3: 'https://qhipapachacode.gitlab.io/qhipapachacode/',
    title: "Pokedex App",
    text: "Pokedex App (React Native)",
  },
  {
    id: 3,
    category: "Swift",
    image: "https://i.imgur.com/LGKGrQV.png",
    link1: "https://gitlab.com/swift20/proyecto-bootcamp/qp-code-famous",
    link2: "https://www.youtube.com/watch?v=sgy3FrOZRmE",
    // link3: 'https://qhipapachacode.gitlab.io/qhipapachacode/',
    title: "QP Code Devs",
    text: "QP Code Devs (Swift-UIKit)",
  },
  {
    id: 4,
    category: "Swift",
    image: "https://i.imgur.com/4w7I8r3.png",
    link1:
      "https://gitlab.com/swift20/platzi/swiftui-games/game-stream-swiftui",
    link2: "https://www.youtube.com/watch?v=LwAT912Kuj0",
    // link3: 'https://qhipapachacode.gitlab.io/qhipapachacode/',
    title: "Games Stream",
    text: "Games Stream (Swift-SwiftUI)",
  },
  {
    id: 5,
    category: "React",
    image: "https://i.imgur.com/Agj9LMC.png",
    // link1: 'https://gitlab.com/vue-js6/vue2-platzi-basico-ia/platzi-cripto-exchange',
    link2: "https://www.youtube.com/watch?v=9-952TDDP2U",
    link3: "https://qpcode.netlify.app/",
    title: "Qhipa Pacha Code",
    text: "Qhipa Pacha Code (React)",
  },
  {
    id: 6,
    category: "Vue",
    image: "https://i.imgur.com/UnACB8g.png",
    link1:
      "https://gitlab.com/vue-js6/vue2-platzi-basico-ia/platzi-cripto-exchange",
    link2: "https://www.youtube.com/watch?v=8q7mAVVwNgE",
    link3: "https://cripto-exchange-jv.netlify.app/",
    title: "Platzi Critpo Exchange",
    text: "Platzi Critpo Exchange (Vue)",
  },
  {
    id: 7,
    category: "React",
    image: "https://i.imgur.com/isirPdt.png",
    link1:
      "https://gitlab.com/proyecto-reactjs/react-platzi-avanzado-md/petgram",
    link2: "https://www.youtube.com/watch?v=n93KbO-1-kY",
    link3: "https://petgram-jorge-vicuna-jorge-vicuna.vercel.app/",
    title: "Petgram",
    text: "Petgram (React)",
  },
  {
    id: 8,
    category: "React",
    image: "https://i.imgur.com/tMXonSE.png",
    link1:
      "https://gitlab.com/proyecto-reactjs/react-platziescuelajs-practico-ot/platzivideo",
    link2: "https://www.youtube.com/watch?v=oELFNug8drU",
    link3: "https://animevideoplatzi-jorge-vicuna.vercel.app/",
    title: "Anime Video Platzi",
    text: "Anime Video Platzi (React)",
  },
  {
    id: 9,
    category: "React",
    image: "https://i.imgur.com/zBQIWdv.png",
    link1:
      "https://gitlab.com/proyecto-reactjs/react_hooks_mern_an/02-movies-app",
    link2: "https://www.youtube.com/watch?v=LnPEKBgs2HA",
    link3:
      "https://proyecto-reactjs.gitlab.io/react_hooks_mern_an/02-movies-app/",
    title: "Movies App",
    text: "Movies App (React)",
  },
  {
    id: 10,
    category: "Flutter",
    image: "https://i.imgur.com/xmYsTQE.png",
    link1: "https://gitlab.com/proyectos-flutter1/proyectos/ja-peru",
    link2: "https://www.youtube.com/watch?v=gAdBpv6uF9g",
    link3:
      "https://play.google.com/store/apps/details?id=com.JuniorAchievement.ja_peru",
    title: "Flutter Ja Perú App",
    text: "Ja Perú App (Flutter)",
  },
  // {
  //     id: 5,
  //     category: 'Animation',
  //     image: img2,
  //     link1: 'https://www.google.com',
  //     link2: 'https://www.google.com',
  //     link3: 'https://play.google.com/store/apps/details?id=com.JuniorAchievement.ja_peru',
  //     title: 'How To Use Blender',
  //     text: 'Free Animation Software'
  // },
  // {
  //     id: 6,
  //     category: 'React JS',
  //     image: react1,
  //     link1: 'https://www.google.com',
  //     link2: 'https://www.google.com',
  //     link3: 'https://play.google.com/store/apps/details?id=com.JuniorAchievement.ja_peru',
  //     title: 'Responsive Portfolio Website',
  //     text: 'Created using different technologies such as Material UI, Styled-Components and more...'
  // },
  // {
  //     id: 7,
  //     category: 'CSS',
  //     image: css2,
  //     link1: 'https://www.google.com',
  //     link2: 'https://www.google.com',
  //     link3: 'https://play.google.com/store/apps/details?id=com.JuniorAchievement.ja_peru',
  //     title: 'Microsoft Website Clone',
  //     text: 'Created using HTML and CSS'
  // }
];

export default portfolios;
