import blog1 from  '../img/blogs/blog1.svg';
import blog3 from  '../img/blogs/blog3.svg';
import blog2 from  '../img/blogs/blog2.svg';
import blog4 from  '../img/blogs/blog4.svg';

// const blogs = [
//     {
//         id: 1,
//         title: 'How To Work from Home',
//         date: '01',
//         month: 'April',
//         image: blog1
//     },
//     {
//         id: 2,
//         title: 'How To Use SEO Efficiently',
//         date: '01',
//         month: 'April',
//         image: blog2
//     },
//     {
//         id: 3,
//         title: 'How to choose a programming Language',
//         date: '01',
//         month: 'April',
//         image: blog3,
//         link: 'https://www.google.co.uk/'
//     },
//     {
//         id: 4,
//         title: 'How To Tse SEO Efficiently',
//         date: '01',
//         month: 'April',
//         image: blog4
//     },
//     {
//         id: 5,
//         title: 'How To Tse SEO Efficiently',
//         date: '01',
//         month: 'April',
//         image: blog4
//     }
// ]

const blogs = [
    {
        id: 1,
        title: 'Estudiantes crean robot para hacer respetar el uso de mascarillas en espacios públicos',
        prensa: 'RPP',
        link: "https://rpp.pe/campanas/valor-compartido/estudiantes-crean-robot-para-hacer-respetar-el-uso-de-mascarillas-en-espacios-publicos-noticia-1307741?fbclid=IwAR26UI9c7xtq_A-OGXntQ_oQKfOLAy_tooodddBPSSpAvz4l94-S-LoPrcM",
        year: '2020',
        image: 'https://e.rpp-noticias.io/normal/2020/12/03/263326_1030451.png'
    },
    {
        id: 2,
        title: 'Estudiantes diseñan robot que detecta a personas que no usan mascarilla',
        prensa: 'EL PERUANO',
        link: "https://elperuano.pe/noticia/112632-estudiantes-disenan-robot-que-detecta-a-personas-que-no-usan-mascarilla",
        year: '2020',
        image: 'https://elperuano.pe/fotografia/thumbnail/2020/12/30/000103366M.jpg'
    },
    {
        id: 3,
        title: 'Robótica al servicio de la salud preventiva',
        prensa: 'RADIO NACIONAL',
        link: "https://www.radionacional.com.pe/novedades/nacion-tecno/robotica-al-servicio-de-la-salud-preventiva",
        year: '2020',
        image: 'https://www.radionacional.com.pe/sites/default/files/styles/note/public/notas/robot_jorge_vicuna.jpg?itok=wZdcTDM-'
    },
    {
        id: 4,
        title: 'Estudiantes diseñan robot que permite detectar a personas sin mascarillas',
        prensa: 'Andina',
        link: "https://andina.pe/agencia/noticia-estudiantes-disenan-robot-permite-detectar-a-personas-sin-mascarillas-827187.aspx",
        year: '2020',
        image: 'https://portal.andina.pe/EDPfotografia3/Thumbnail/2020/12/22/000736040W.jpg'
    },
    {
        id: 5,
        title: 'IoTMaskRover”, una iniciativa de nuestros estudiantes triunfa en el concurso nacional IEEE YESIST 12-2020',
        prensa: 'UPN',
        link: "https://www.upn.edu.pe/noticias/iotmaskrover-una-iniciativa-de-nuestros-estudiantes-triunfa-en-el-concurso-nacional-ieee",
        year: '2020',
        image: 'https://www.upn.edu.pe/sites/default/files/2020-10/upn_nw_ing_robot_qhipa_pacha_22-oct.png'
    },
    {
        id: 6,
        title: 'Universitarios crean aplicativo para acceso rápido a historias clínicas',
        prensa: 'Andina',
        link: "https://andina.pe/agencia/noticia-universitarios-crean-aplicativo-para-acceso-rapido-a-historias-clinicas-776116.aspx",
        year: '2019',
        image: 'https://portal.andina.pe/EDPfotografia3/Thumbnail/2019/11/24/000636095W.jpg'
    },
    {
        id: 7,
        title: 'HACKATHON LA POSITIVA - QHIPA PACHA',
        prensa: 'UPN',
        link: "https://www.upn.edu.pe/historias-estudiante/hackathon-la-positiva-qhipa-pacha",
        year: '2019',
        image: 'https://www.upn.edu.pe/sites/default/files/styles/large/public/historia/hackathon_la_positiva.jpg?itok=9T7RbUaL'
    },
    {
        id: 8,
        title: 'CADE UNIVERSITARIO: ESTUDIANTES UPN DESTACARON EN INICIATIVA ALIANZA POR LOS JÓVENES DE NESTLÉ',
        prensa: 'BLOG UPN',
        link: "https://blogs.upn.edu.pe/mividaenupn/2019/09/18/cade-universitario-estudiantes-upn-destacaron-en-iniciativa-alianza-por-los-jovenes-de-nestle/",
        year: '2019',
        image: 'https://i.imgur.com/zQp5ldH.jpg'
    },
    {
        id: 9,
        title: 'UPN: Estudiantes de Ingeniería de la Universidad Privada del Norte crean aplicativo para acceso rápido a historias clínicas',
        prensa: 'EDUCACIONENRED',
        link: "https://noticia.educacionenred.pe/2019/11/upn-estudiantes-ingenieria-universidad-privada-norte-crean-aplicativo-acceso-rapido-187068.html",
        year: '2019',
        image: 'https://3.bp.blogspot.com/-wpK93f6BSVY/XdyuF96aYXI/AAAAAAAA0nU/qaZTfeXF5jEqayNwXMA92JWUcPTnnZvTACLcBGAsYHQ/w851-h444-c/187068-upn-estudiantes-ingenieria-universidad-privada-norte-crean-aplicativo-acceso.jpg'
    },
    {
        id: 10,
        title: 'Estudiantes inventan dispositivo que alerta si chofer se encuentra en estado etílico',
        prensa: 'ANDINA',
        link: "https://andina.pe/agencia/seccion-clic-35.aspx/publicidad/publicidad/noticia-estudiantes-inventan-dispositivo-alerta-si-chofer-se-encuentra-estado-etilico-767151.aspx",
        year: '2018',
        image: 'https://www.nitro.pe/images/2018/noviembre/alumnos_concurso.jpg'
    },
    {
        id: 11,
        title: 'EQUIPO UPN OBTIENE EL SEGUNDO LUGAR EN HACKATHON LA POSITIVA',
        prensa: 'BLOG UPN',
        link: "https://blogs.upn.edu.pe/ingenieria/2018/09/27/equipo-upn-obtiene-el-segundo-lugar-en-hackathon-la-positiva/",
        year: '2018',
        image: "https://www.lapositiva.com.pe/wps/wcm/connect/corporativo/4b2226b4-2802-460a-b292-13afcc06f1d4/1/hachaton-1.jpg?MOD=AJPERES&CVID=",
    },
    {
        id: 12,
        title: 'Perú: estudiantes crean dispositivo para prevenir accidentes de tránsito',
        prensa: 'NITRO',
        link: "https://www.nitro.pe/seguridad/peru-estudiantes-crean-dispositivo-para-prevenir-accidentes-de-transito.html",
        year: '2018',
        image: 'https://www.nitro.pe/images/2018/noviembre/alumnos_concurso.jpg'
    }
]

export default blogs;