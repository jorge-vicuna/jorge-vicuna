# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

npm install styled-components
npm install react-router-dom
npm install react-particles-js (requiere npm i tsparticles)
npm i tsparticles
npm install @material-ui/core
npm install @material-ui/icons
npm install react-icons
npm install --save-dev sass

### Para usar Gitlab pages:

(https://jorge-vicuna.gitlab.io/jorge-vicuna/)

- #### Basename que esta en AppRouter:

  - `<BrowserRouter basename= "/jorge-vicuna/">" >`
  - `/jorge-vicuna/` reemplazarlo por tu Url base

####

- #### Colocarlo en el package.json:

  - `"homepage": "https://jorge-vicuna.gitlab.io/jorge-vicuna/"`,
  - `/jorge-vicuna/` reemplazarlo por tu Url base

####

- #### Esta documentación me ayudo:

  (https://create-react-app.dev/docs/deployment/#building-for-relative-paths)
